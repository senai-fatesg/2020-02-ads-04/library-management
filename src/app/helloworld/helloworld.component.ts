import { Component, OnInit } from '@angular/core';
import { CountryService } from './country-service';

interface Book {
  name: string;
  author: string;
 }


@Component({
  selector: 'app-helloworld',
  templateUrl: './helloworld.component.html',
  styleUrls: ['./helloworld.component.css']
})
export class HelloworldComponent implements OnInit {

  books: Book[];
  selectedBook: string;

  selectedCountry: any;

  filteredCountries: any[];

  countries: any[];

  value: Date;

  values2: string[];
  

  constructor(private countryService: CountryService) {
    this.books = [
      {name: 'Book1', author: 'Author1'},
      {name: 'Book2', author: 'Author2'},
      {name: 'Book3', author: 'Author3'},
      {name: 'Book4', author: 'Author4'},
      {name: 'Book5', author: 'Author5'}
      ];
      }


  ngOnInit() {
    this.countryService.getCountries().subscribe(r => {
      console.log(r.data);
      this.countries = r.data;
    });
  }

  filterCountry(event) {
    console.log(event)
    //in a real application, make a request to a remote url with the query and return filtered results, for demo we filter at client side
    let filtered : any[] = [];
    let query = event.query;
    for(let i = 0; i < this.countries.length; i++) {
        let country = this.countries[i];
        if (country.name.toLowerCase().indexOf(query.toLowerCase()) == 0) {
            filtered.push(country);
        }
    }
    
    this.filteredCountries = filtered;
}  

}
